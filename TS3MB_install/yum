#!/bin/bash

source link.list
source colors

NO_OUTPUT="1"
printf " ${INFO} Step 1: Cleanup \n"
spinner & spinner_background "yum clean all" "yum clean all"

printf " ${INFO} Step 2: Update Packages \n"
spinner & spinner_background "yum -q -y update" "yum update"

printf " ${INFO} Step 3: Upgrade Packages \n"
spinner & spinner_background "yum -q -y upgrade" "yum upgrade"

printf " ${INFO} Step 4: Remove unused packages \n"
spinner & spinner_background "yum -q -y autoremove" "yum autoremove"

printf " ${INFO} Step 5: Cleanup \n"
spinner & spinner_background "yum clean all" "yum clean all"

NO_OUTPUT=""
echo

printf " ${INFO} Check required dependencies \n"

# Package Sources import
if [ "$NOTPORTABLE" = "1" ]; then
	if [ "$OS_VERSION" = "7" ]; then
		printf " ${INFO} Install Package: epel-release \n"
		if rpm -q epel-release &> /dev/null; then
			good_edit
		else
			NO_OUTPUT="1"
			spinner & spinner_background "yum install -y epel-release &> /dev/null" "yum install -y epel-release"
			NO_OUTPUT=""
		fi

		printf " ${INFO} Install Package: nux-dextop-release \n"
		if rpm -q nux-dextop-release &> /dev/null; then
			good_edit
		else
			NO_OUTPUT="1"
			spinner & spinner_background "rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm &> /dev/null" "yum install -y epel-release"
			NO_OUTPUT=""
		fi

		printf " ${INFO} Import GPG KEY \n"
		NO_OUTPUT="1"
		spinner & spinner_background "rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro" "rpm --import"
		NO_OUTPUT=""
	else
		echo " ERROR!"
		exit 1
	fi
fi

PACKAGE=""
PACKAGECOUNTER="0"
PACKETLISTJAVA=()
PACKETLIST=()


if [ "$PORTABLE" = "1" ] || [ "$NOTPORTABLE" = "1" ]; then
	############
	##  Java  ##
	############
	PACKETLISTJAVA=( java-11-openjdk java-1.8.0-openjdk java-1.7.0-openjdk )
	for PACKAGE in "${PACKETLISTJAVA[@]}"
	do
		if rpm -q $PACKAGE &> /dev/null; then
			printf " ${GOOD} $PACKAGE installed. \n"
			JAVAINSTALLED="1"
		fi
	done

	if ! [ "$JAVAINSTALLED" = "1" ]; then
		PACKETLISTJAVA=( java-11-openjdk java-1.8.0-openjdk java-1.7.0-openjdk )
		for PACKAGE in "${PACKETLISTJAVA[@]}"
		do
			if yum list java-* | grep -qm 1 "$PACKAGE"; then
				JAVAVERSION="${PACKAGE}"

				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
				break
			fi
		done
	fi
	

	############
	##  wget  ##
	############
	PACKAGE="wget"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##############
	##  screen  ##
	##############
	PACKAGE="screen"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi

fi

############################
############################
##                        ##
##  NotPortable Packages  ##
##                        ##
############################
############################
if [ "$NOTPORTABLE" = "1" ]; then

	###############
	##  mplayer  ##
	###############
	PACKAGE="mplayer"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##############
	##  ffmpeg  ##
	##############
	PACKAGE="ffmpeg-devel"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi
	PACKAGE="ffmpeg"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	###############
	##  xdotool  ##
	###############
	PACKAGE="xdotool"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libx11-xcb-dev  ##
	######################
	PACKAGE="libX11-devel"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libxcursor-dev  ##
	######################
	PACKAGE="libXcursor-devel"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libxrender-dev  ##
	######################
	PACKAGE="libXrender-devel"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	############
	##  xvfb  ##
	############
	PACKAGE="xorg-x11-server-Xvfb"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	#################
	##  x11-utils  ##
	#################
	PACKAGE="xorg-x11-utils"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##############
	##  python  ##
	##############
	PACKAGE="python"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##################
	##  libxrandr2  ##
	##################
	PACKAGE="libXrandr"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	####################
	##  libglib2.0-0  ##
	####################
	# in package ffmpeg


	####################
	##  libegl1-mesa  ##
	####################
	PACKAGE="mesa-libEGL"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	########################
	##  libavcodec-extra  ##
	########################
	# in package ffmpeg


	########################
	##  mesa-dri-drivers  ##
	########################
	PACKAGE="mesa-dri-drivers"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	#####################
	##  libsdl2-2.0-0  ##
	#####################
	# unused


	#####################
	##  pciutils-libs  ##
	#####################
	PACKAGE="pciutils-libs"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	###############
	##  libxslt  ##
	###############
	PACKAGE="libxslt"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	#####################
	##  libXScrnSaver  ##
	#####################
	PACKAGE="libXScrnSaver"
	if rpm -q $PACKAGE > /dev/null; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi

fi


###################
###################
##               ##
##    Install    ##
##               ##
###################
###################
if ! [ "${PACKAGECOUNTER}" = 0 ]; then
	count=0
	LIST=""
	while ! [ "$count" = "${#PACKAGELISTINSTALL[@]}" ]; do
		LIST+="${PACKAGELISTINSTALL[$count]}\n"
		(( count += 1 ))
	done
	if (whiptail --title "Install" --yesno "Install this Packages?:\n\n${LIST}" 24 30); then
		echo
		printf " ${INFO} Install Now\n"
	else
		printf " Aborted! Nothing Installed.\n"
		exit 1
	fi
	echo
	if [ "$REPLY" = "n" ] || [ "$REPLY" = "N" ]; then
		printf " Aborted! Nothing Installed."
		exit 1
	fi
	bold " ${YELLOW}${INFO}${MAIN} ! Important: Please do not cancel the installation !"
	sleep 2
	echo

	for PACKAGE in "${PACKAGELISTINSTALL[@]}"
	do
		printf " ${INFO} Install Package: $PACKAGE \n"
		NO_OUTPUT="1"
		spinner & spinner_background "yum install -y ${PACKAGE}" "yum install -y ${PACKAGE}"
		NO_OUTPUT=""
	done
	echo
	printf " ${GOOD} Install Succesful.\n"
fi

sleep 3
echo
exit 0