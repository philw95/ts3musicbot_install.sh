#!/bin/bash

source link.list
source colors

NO_OUTPUT="1"
printf " ${INFO} Step 1: pre-configuring packages \n"
spinner & spinner_background "dpkg --configure -a" "dpkg --configure -a"

printf " ${INFO} Step 2: Fix and attempt to correct a system with broken dependencies \n"
spinner & spinner_background "apt-get install -f" "apt-get install -f"

printf " ${INFO} Step 3: Update apt cache \n"
spinner & spinner_background "apt-get update" "apt-get update"

printf " ${INFO} Step 4: Upgrade packages \n"
spinner & spinner_background "apt-get upgrade -y" "apt-get upgrade"

printf " ${INFO} Step 5: Distribution upgrade \n"
spinner & spinner_background "apt-get dist-upgrade -y" "apt-get dist-upgrade"

printf " ${INFO} Step 6: Remove unused packages \n"
spinner & spinner_background "apt-get --purge autoremove -y" "apt-get --purge autoremove"

printf " ${INFO} Step 7: Clean up \n"
spinner & spinner_background "apt-get autoclean -y" "apt-get autoclean"

NO_OUTPUT=""
echo

printf " ${INFO} Check required dependencies \n"

# Package Sources import
# if [ "$NOTPORTABLE" = "1" ]; then
	
# fi

PACKAGE=""
PACKAGECOUNTER="0"
MPLAYER_D8=""
FFMPEG_D=""
FFMPEG_U=""
PACKETLISTJAVA=()
PACKETLIST=()

if [ "$PORTABLE" = "1" ] || [ "$NOTPORTABLE" = "1" ]; then
	############
	##  Java  ##
	############
	PACKETLISTJAVA=( openjdk-11-jre openjdk-10-jre openjdk-9-jre openjdk-8-jre openjdk-7-jre )
	for PACKAGE in "${PACKETLISTJAVA[@]}"
	do
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			JAVAINSTALLED="1"
			printf " ${GOOD} $PACKAGE installed. \n"
		fi
	done

	if ! [ "$JAVAINSTALLED" = "1" ]; then
		PACKETLISTJAVA=( openjdk-11-jre openjdk-8-jre openjdk-7-jre )
		for PACKAGE in "${PACKETLISTJAVA[@]}"
		do
			if apt-cache search openjdk | grep -qo "$PACKAGE "; then
				JAVAVERSION="${PACKAGE}"

				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
				break
			fi
		done
	fi


	############
	##  wget  ##
	############
	PACKAGE="wget"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##############
	##  screen  ##
	##############
	PACKAGE="screen"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	###############
	##  locales  ##
	###############
	PACKAGE="locales"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi

fi

############################
############################
##                        ##
##  NotPortable Packages  ##
##                        ##
############################
############################
if [ "$NOTPORTABLE" = "1" ]; then

	###############
	##  mplayer  ##
	###############
	PACKAGE="mplayer"
	if [ "$OS" = "Debian" ] && [ "$OS_VERSION" = "8" ]; then
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			if dpkg -l | grep -E 'ii|hi' | grep mplayer | grep TS3MusicBot.net > /dev/null; then
				printf " ${GOOD} $PACKAGE installed. \n"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
				(( PACKAGECOUNTER += 1 ))
				MPLAYER_D8="1"
			fi
		else
			printf " ${ERROR} $PACKAGE not installed. \n"
			(( PACKAGECOUNTER += 1 ))
			MPLAYER_D8="1"
		fi
	else
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			printf " ${GOOD} $PACKAGE installed. \n"
		else
			if [ "$OS" = "Debian" ] && [ "$OS_VERSION" = "9" ] || [ "$OS_VERSION" = "buster/sid" ]; then
				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
			elif [ "$OS" = "Ubuntu" ]; then
				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
			fi
		fi
	fi


	##############
	##  ffmpeg  ##
	##############
	PACKAGE="ffmpeg"
	if [ "$OS" = "Ubuntu" ]; then
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			if [ "$OS_VERSION" = "14.04" ] || [ "$OS_VERSION" = "16.04" ] || [ "$OS_VERSION" = "18.04" ]; then
				if dpkg -l | grep -E 'ii|hi' | grep ffmpeg | grep TS3MusicBot.net > /dev/null; then
					printf " ${GOOD} $PACKAGE installed. \n"
				else
					printf " ${ERROR} $PACKAGE not installed. \n"
					(( PACKAGECOUNTER += 1 ))
					FFMPEG_U="1"
				fi
			fi
		else
			if [ "$OS_VERSION" = "14.04" ] || [ "$OS_VERSION" = "16.04" ] || [ "$OS_VERSION" = "18.04" ]; then
				printf " ${ERROR} $PACKAGE not installed. \n"
				(( PACKAGECOUNTER += 1 ))
				FFMPEG_U="1"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
			fi
		fi
	elif [ "$OS" = "Debian" ]; then
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			if [ "$OS_VERSION" = "8" ] || [ "$OS_VERSION" = "9" ]; then
				if dpkg -l | grep -E 'ii|hi' | grep ffmpeg | grep TS3MusicBot.net > /dev/null; then
					printf " ${GOOD} $PACKAGE installed. \n"
				else
					printf " ${ERROR} $PACKAGE not installed. \n"
					(( PACKAGECOUNTER += 1 ))
					FFMPEG_D="1"
				fi
			else
				printf " ${GOOD} $PACKAGE installed. \n"
			fi
		else
			if [ "$OS_VERSION" = "8" ] || [ "$OS_VERSION" = "9" ]; then
				printf " ${ERROR} $PACKAGE not installed. \n"
				(( PACKAGECOUNTER += 1 ))
				FFMPEG_D="1"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
				PACKAGELISTINSTALL+=($PACKAGE)
				(( PACKAGECOUNTER += 1 ))
			fi
		fi
	fi


	###############
	##  xdotool  ##
	###############
	PACKAGE="xdotool"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libx11-xcb-dev  ##
	######################
	PACKAGE="libx11-xcb-dev"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libxcursor-dev  ##
	######################
	PACKAGE="libxcursor-dev"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	######################
	##  libxrender-dev  ##
	######################
	PACKAGE="libxrender-dev"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	############
	##  xvfb  ##
	############
	PACKAGE="xvfb"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	#################
	##  x11-utils  ##
	#################
	PACKAGE="x11-utils"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##############
	##  python  ##
	##############
	PACKAGE="python"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##################
	##  libxrandr2  ##
	##################
	PACKAGE="libxrandr2"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	####################
	##  libglib2.0-0  ##
	####################
	PACKAGE="libglib2.0-0"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	####################
	##  libegl1-mesa  ##
	####################
	PACKAGE="libegl1-mesa"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	########################
	##  libavcodec-extra  ##
	########################
	if [ "$OS" = "Debian" ] && [ "$OS_VERSION" = "8" ]; then
		LIBAVCODEC_D8="1"
	else
		PACKAGE="libavcodec-extra"
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			printf " ${GOOD} $PACKAGE installed. \n"
		else
			printf " ${ERROR} $PACKAGE not installed. \n"
			PACKAGELISTINSTALL+=($PACKAGE)
			(( PACKAGECOUNTER += 1 ))
		fi
	fi


	########################
	##  mesa-dri-drivers  ##
	########################
	# unused

	#####################
	##  libsdl2-2.0-0  ##
	#####################
	if [ "$OS" = "Ubuntu" ] && [ "$OS_VERSION" = "14.04" ]; then
		PACKAGE="libsdl2-2.0-0"
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
			printf " ${GOOD} $PACKAGE installed. \n"
		else
			printf " ${ERROR} $PACKAGE not installed. \n"
			PACKAGELISTINSTALL+=($PACKAGE)
			(( PACKAGECOUNTER += 1 ))
		fi
	fi


	###############
	##  libpci3  ##
	###############
	PACKAGE="libpci3"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	##################
	##  libxslt1.1  ##
	##################
	PACKAGE="libxslt1.1"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi


	###############
	##  libxss1  ##
	###############
	PACKAGE="libxss1"
	if dpkg-query -W -f='${Status} ${Version}\n' $PACKAGE 2> /dev/null | grep -qw "ok installed"; then
		printf " ${GOOD} $PACKAGE installed. \n"
	else
		printf " ${ERROR} $PACKAGE not installed. \n"
		PACKAGELISTINSTALL+=($PACKAGE)
		(( PACKAGECOUNTER += 1 ))
	fi

fi


###################
###################
##               ##
##    Install    ##
##               ##
###################
###################
if ! [ "${PACKAGECOUNTER}" = 0 ]; then
	count=0
	LIST=""
	while ! [ "$count" = "${#PACKAGELISTINSTALL[@]}" ]; do
		LIST+="${PACKAGELISTINSTALL[$count]}\n"
		(( count += 1 ))
	done
	if (whiptail --title "Install" --yesno "Install this Packages?:\n\n${LIST}" 24 30); then
		echo
		printf " ${INFO} Install Now\n"
	else
		printf " Aborted! Nothing Installed.\n"
		exit 1
	fi
	echo
	if [ "$REPLY" = "n" ] || [ "$REPLY" = "N" ]; then
		printf " Aborted! Nothing Installed."
		exit 1
	fi
	bold " ${YELLOW}${INFO}${MAIN} ! Important: Please do not cancel the installation !"
	sleep 2
	echo

	if [ "$NOTPORTABLE" = "1" ]; then
		if [ "$MPLAYER_D8" = "1" ]; then
			if [ "$LIBAVCODEC_D8" = "1" ]; then
				mv TS3MusicBot_Debian8.list /etc/apt/sources.list.d/
				apt-get install -y libavcodec-extra57
				rm /etc/apt/sources.list.d/TS3MusicBot_Debian8.list
				apt-get update
			fi
			apt-get -f install -y libaa1-dev libasound2-dev libcaca-dev libcdparanoia-dev libdca-dev \
			libdirectfb-dev libenca-dev libesd0-dev libfontconfig1-dev libfreetype6-dev \
			libfribidi-dev libgif-dev libgl1-mesa-dev libjack-jackd2-dev libopenal1 libpulse-dev \
			libsdl1.2-dev libvdpau-dev libxinerama-dev libxv-dev libxvmc-dev libxxf86dga-dev \
			libxxf86vm-dev librtmp-dev libsctp-dev libass-dev libsmbclient-dev libtheora-dev \
			libogg-dev libxvidcore-dev libspeex-dev libvpx-dev libschroedinger-dev libdirac-dev libdv4-dev \
			libopencore-amrnb-dev libopencore-amrwb-dev libmp3lame-dev liblivemedia-dev libtwolame-dev \
			libmad0-dev libgsm1-dev libbs2b-dev liblzo2-dev ladspa-sdk libopenjpeg-dev libfaad-dev \
			libmpg123-dev libbluray-dev libaacs-dev subversion
			wget ${MPLAYER_D8_DOWNLOADLINK}
			dpkg -i ${MPLAYER_D8_PACKAGE}
			rm ${MPLAYER_D8_PACKAGE}
		fi
		if [ "$FFMPEG_D" = "1" ]; then
			apt-get -f install -y libmp3lame-dev libssl-dev
			if [ "$OS_VERSION" = "8" ]; then
				wget ${FFMPEG_D8_DOWNLOADLINK}
				dpkg -i ${FFMPEG_D8_PACKAGE}
				echo "ffmpeg hold"|dpkg --set-selections
				rm ${FFMPEG_D8_PACKAGE}
			elif [ "$OS_VERSION" = "9" ]; then
				wget ${FFMPEG_D9_DOWNLOADLINK}
				dpkg -i ${FFMPEG_D9_PACKAGE}
				echo "ffmpeg hold"|dpkg --set-selections
				rm ${FFMPEG_D9_PACKAGE}
			fi
		fi
		if [ "$FFMPEG_U" = "1" ]; then
			apt-get -f install -y libmp3lame-dev libssl-dev
			if [ "$OS_VERSION" = "14.04" ] || [ "$OS_VERSION" = "16.04" ] || [ "$OS_VERSION" = "18.04" ]; then
				wget ${FFMPEG_U1404_DOWNLOADLINK}
				dpkg -i ${FFMPEG_U1404_PACKAGE}
				echo "ffmpeg hold"|dpkg --set-selections
				rm ${FFMPEG_U1404_PACKAGE}
			fi
		fi

		echo ""
		if [ "$MPLAYER_D8" = "1" ] || [ "$LIBAVCODEC_D8" = "1" ]; then
			if dpkg-query -W -f='${Status} ${Version}\n' mplayer 2> /dev/null | grep -qw "ok installed"; then
				printf " ${GOOD} $PACKAGE installed. \n"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
			fi
			if dpkg-query -W -f='${Status} ${Version}\n' libavcodec-extra57 2> /dev/null | grep -qw "ok installed"; then
				printf " ${GOOD} $PACKAGE installed. \n"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
			fi
		fi

		if [ "$FFMPEG_D" = "1" ] || [ "$FFMPEG_U" = "1" ]; then
			if dpkg-query -W -f='${Status} ${Version}\n' ffmpeg 2> /dev/null | grep -qw "ok installed"; then
				printf " ${GOOD} $PACKAGE installed. \n"
			else
				printf " ${ERROR} $PACKAGE not installed. \n"
			fi
		fi
	fi

	for PACKAGE in "${PACKAGELISTINSTALL[@]}"
	do
		printf " ${INFO} Install Package: $PACKAGE \n"
		NO_OUTPUT="1"
		spinner & spinner_background "apt-get install -y ${PACKAGE}" "apt-get install -y ${PACKAGE}"
		NO_OUTPUT=""
	done
	echo
	printf " ${GOOD} Install Succesful.\n"
fi

sleep 3
echo
exit 0