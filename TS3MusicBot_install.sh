#!/bin/bash

VERSION="0.0.7"
PROJECT_SIDE="https://gitlab.com/philw95/ts3musicbot_install.sh"
INSTALL_ARCHIV_MASTER="${PROJECT_SIDE}/raw/release/TS3MusicBot_install.tar"
INSTALL_ARCHIV_BETA="${PROJECT_SIDE}/raw/beta_release/TS3MusicBot_install.tar"
INSTALL_ARCHIV_DEVELOP="${PROJECT_SIDE}/raw/develop_release/TS3MusicBot_install.tar"

# Description:
#	This install Script install all needed Packages for the TS3MusicBot.
#
# Works with:
#	Debian: 10,9,8
#	Ubuntu: 18.04,16.04,14.04
#	CentOS: 7
#

################
################

#     TEST

################

echo " ######################################"
echo "             TEST"
echo
echo " Warte auf eingabe: "
read TEST

echo " \$TEST= $TEST"

echo
echo " ###### ENDE #####"
exit 0








################
################

# TS3MusicBot:
# Executive Director: Nico Sprang
# Website: www.ts3musicbot.net
# Copyright 2012-2019 TS3MusicBot
# "TS3MusicBot" is a registered trademark of TS3MusicBot.net.
#
# This Scipt: TS3MusicBot_install.sh
# Script creator: Philippe Wagner
# Project Side: see PROJECT_SIDE
# Creation Date: 04.01.2018
# Copyright 2019 TS3MusicBot_install.sh
# License: see PROJECT_SIDE

case "$1" in
	-master)
		INSTALL_ARCHIV="${INSTALL_ARCHIV_MASTER}"
	;;
	"")
		INSTALL_ARCHIV="${INSTALL_ARCHIV_MASTER}"
	;;
	-beta)
		INSTALL_ARCHIV="${INSTALL_ARCHIV_BETA}"
	;;
	-develop)
		INSTALL_ARCHIV="${INSTALL_ARCHIV_DEVELOP}"
	;;
	-help)
		echo "Available Parameters:"
		echo ""
		echo "-master        Stable Release"
		echo "-beta          Beta Release"
	;;
	*)
		echo "Incorrect input! Use: ${0} -help"
		exit 1
	;;
esac

# download main script
wget ${INSTALL_ARCHIV} 2> /dev/null
if [ -e TS3MusicBot_install.tar ]; then
	rm TS3MusicBot_install.sh
	tar xfv TS3MusicBot_install.tar
	rm TS3MusicBot_install.tar
	chmod +x TS3MusicBot_install.sh
	cd files
	chmod +x TS3MusicBot_install_Main TS3MusicBot_install_Package TS3MusicBot_install_Bot
	exec ./TS3MusicBot_install_Main
else
	echo "ERROR: Can't download TS3MusicBot_install.tar from:"
	echo ${INSTALL_ARCHIV}
	exit 1
fi

exit 0