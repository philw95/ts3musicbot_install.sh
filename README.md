[![current_version](https://img.shields.io/badge/Version-0.0.7-green.svg)](https://gitlab.com/philw95/ts3musicbot_install.sh/tags) [![license](https://img.shields.io/badge/License-MIT_License-red.svg)](LICENSE)
Master: [![pipeline status master](https://gitlab.com/philw95/ts3musicbot_install.sh/badges/master/pipeline.svg)](https://gitlab.com/philw95/ts3musicbot_install.sh/commits/master)
Beta: [![pipeline status beta](https://gitlab.com/philw95/ts3musicbot_install.sh/badges/beta/pipeline.svg)](https://gitlab.com/philw95/ts3musicbot_install.sh/commits/beta)
Develop: [![pipeline status develop](https://gitlab.com/philw95/ts3musicbot_install.sh/badges/develop/pipeline.svg)](https://gitlab.com/philw95/ts3musicbot_install.sh/commits/develop)

---

[![TS3MusicBot_Logo](http://ts3musicbot.net/linkus/ts3musicbot1.png)](https://www.ts3musicbot.net)

---

# Table of Content

### [1. Attention](#1-attention-)

### [2. About](#2-about-)

### [3. Usage](#3-usage-)

### [4. Parameter](#4-parameter-)

### [5. Versions](#5-versions-)

### [6. Supportet OS](#6-supportet-operating-systems-)

### [7. Latest Changes](#7-latest-changes-)

### [8. Support](#8-support-)

---
&nbsp;  

# 1. Attention [↑](#table-of-content)

### I assume no liability for damages to the server!

### A license is required to use the bot! Buy license here: https://ts3musicbot.net

---
&nbsp;  

# 2. About [↑](#table-of-content)

TS3MusicBot.net:
> You ever wanted to play music on your teamspeak or discord server? Now you can!
> TS3MusicBot is a unique feature for your teamspeak or discord server fully working on linux and windows.
> 
> Upload music files, manage folders, play all kind of music files, stream live internet radio stations, direct playback of youtube, soundcloud and more links.
> The TS3MusicBot can be controlled with chat commands or with the build in webinterface.
> 
> Listen to music in groups while playing your favorite game. Let your friends listen to a youtube video you found.
> A new experience to listen music live with others in the same channel.

This Script:

**Basic installation for TS3MusicBot in:**

- Portable Mode
- Notportable Mode
- ~~Docker~~ (Will be added in one of the next updates)

You can download the Bot Files or My Stript (TS3MusicBot_Control: https://gitlab.com/philw95/TS3MB_Script) over this Install script.

~~If you Select Docker it will downloaded this Docker Container: https://hub.docker.com/r/ts3mb/ts3musicbot (Created by Marco Eckert)~~


---
&nbsp;  

# 3. Usage [↑](#table-of-content)

Download the script and make it executable:
```
wget https://gitlab.com/philw95/ts3musicbot_install.sh/raw/master/TS3MusicBot_install.sh && chmod +x TS3MusicBot_install.sh
```

Starting the script:
```
./TS3MusicBot_install.sh
```

### It must be started with root rights!

---
&nbsp;  

# 4. Parameter [↑](#table-of-content)

```
-master
-beta
-develop [Don't Use!]
```

---
&nbsp;  

# 5. Versions [↑](#table-of-content)

| Master  | stable version                 |
|---------|--------------------------------|
| Beta    | testing version                |
| Develop | Developer Version [Don't Use!] |

---
&nbsp;  

# 6. Supportet Operating Systems [↑](#table-of-content)

| Debian | Ubuntu | Cent OS |
|:------:|:------:|:-------:|
|   10   |  18.04 |    7    |
|    9   |  16.04 |         |
|    8   |  14.04 |         |

---
&nbsp;  

# 7. Latest Changes [↑](#table-of-content)

### Version 0.0.7

*  Add. Add TS3MB_Script Download Link

### Version 0.0.6

*  Fix. The Botfiles and the Script was Downloaded in the Wrong folder. Now Fixed.

### Version 0.0.5

*  Add Beta and Develop Mode


### Full Changelog: https://gitlab.com/philw95/ts3musicbot_install.sh/blob/master/CHANGELOG

---
&nbsp;  

# 8. Support [↑](#table-of-content)

### TS3MusicBot Livechat/Support: https://livesupport.ts3musicbot.net/chat.php

---
&nbsp;  